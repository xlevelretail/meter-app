import frappe
from frappe import _

def get_data():
	return [
		{	"module_name": "xlevel_meter",
			"label": _("Setup"),
			"icon": "icon-star",
			"items": [
				{	"type": "doctype",
					"name": "Pump Item",
					"label": "Items"
				},
				{	"type": "doctype",
					"name": "Pump",
					"label": "Pump"
				},
			]
		},
		{	"module_name": "xlevel_meter",
			"label": _("Entry"),
			"icon": "icon-star",
			"items": [
				{	"type": "doctype",
					"name": "Pump Meter Reading",
					"label": "Pump Meter Reading"
				}
			]
		},
		{	"module_name": "xlevel_meter",
			"label": _("Entry"),
			"icon": "icon-star",
			"items": [
				{	"type": "report",
					"name": "Pump Sales Commission",
					"doctype": "Pump Meter Reading",
					"is_query_report": True
				}
			]
		},
		{	"module_name": "xlevel_meter",
			"label": _("Entry"),
			"icon": "icon-star",
			"items": [
				{	"type": "doctype",
					"name": "Pump Employee Wages Journal",
					"label": "Employee Wages Journal"
				}
			]
		}
]