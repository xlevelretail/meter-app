// Copyright (c) 2018, hello@openetech.com and contributors
// For license information, please see license.txt

frappe.provide("xlevel_meter.pump_meter_reading");
frappe.ui.form.on("Pump Meter Items", {
	pump_item_code: (frm, cdt, cdn) => {
		xlevel_meter.pump_meter_reading.update_item_rate_wages(frm, cdt, cdn);
		xlevel_meter.pump_meter_reading.update_amount(frm, cdt, cdn);
		
	},
	ending_qty: (frm, cdt, cdn) => {
		xlevel_meter.pump_meter_reading.update_item_rate_wages(frm, cdt, cdn);
		xlevel_meter.pump_meter_reading.update_amount(frm, cdt, cdn);
	}
});

xlevel_meter.pump_meter_reading.update_item_rate_wages = function(frm, cdt, cdn) {
	let entry_list = ['pump_meter_items'];
	var pump = frm.doc.pump;
	entry_list.forEach((entry) => {
		frm.doc[entry].forEach((item, index) => {
			if (item.pump_item_code && item.name){
				frappe.call({
					method:"xlevel_meter.xlevel_meter.doctype.pump_meter_reading.pump_meter_reading.get_rates",
					args:{
						'pump':pump,
						'item_code':item.pump_item_code
					},
					callback:function(r){
						if (r.message){
							var opening_qty, selling_price, comm_per_unit;
							opening_qty = r.message.opening_qty;
							selling_price = r.message.selling_price;
							comm_per_unit = r.message.comm_per_unit
							frappe.model.set_value('Pump Meter Items',item.name,'opening_qty',opening_qty);
							frappe.model.set_value('Pump Meter Items',item.name,'selling_price',selling_price);
							frappe.model.set_value('Pump Meter Items',item.name,'comm_per_unit',comm_per_unit);
							if (!frappe.model.get_value('Pump Meter Items',item.name,'opening_qty'))
								frappe.model.set_value('Pump Meter Items',item.name,'opening_qty',0);
							if (!frappe.model.get_value('Pump Meter Items',item.name,'selling_price'))
								frappe.model.set_value('Pump Meter Items',item.name,'selling_price',0);
							if (!frappe.model.get_value('Pump Meter Items',item.name,'comm_per_unit'))
								frappe.model.set_value('Pump Meter Items',item.name,'comm_per_unit',0);
						}
					}
				})
			}
		});
	});
};

xlevel_meter.pump_meter_reading.update_amount = function(frm, cdt, cdn) {
	let entry_list = ['pump_meter_items'];
	entry_list.forEach((entry) => {
		frm.doc[entry].forEach((item, index) => {
			if (item.pump_item_code && item.ending_qty && item.selling_price){
				var amount;
				amount = Math.round((item.ending_qty - item.opening_qty ) * item.selling_price).toFixed(2);
				frappe.model.set_value('Pump Meter Items',item.name, 'sales_amount',amount)
			}
			if (item.pump_item_code && item.ending_qty && item.comm_per_unit){
				var comm_amt;
				comm_amt = Math.round((item.ending_qty - item.opening_qty ) * item.comm_per_unit).toFixed(2);
				frappe.model.set_value('Pump Meter Items',item.name, 'commision_amount',comm_amt)
			}
		});
	});
};