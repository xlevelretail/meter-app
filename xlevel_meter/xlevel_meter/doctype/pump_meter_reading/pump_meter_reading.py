# -*- coding: utf-8 -*-
# Copyright (c) 2018, hello@openetech.com and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _

class PumpMeterReading(Document):
	def validate(self):
		si_total = 0
		pay_total = 0
		for si in self.pump_meter_items:
			si_total = si_total + float(si.sales_amount)
			pay_total = pay_total + float(si.commision_amount)
			if si.ending_qty < si.opening_qty:
				frappe.throw(_("Opening reading cannot be greater than ending reading"))
			if (si.ending_qty - si.opening_qty) == 0:
				frappe.msgprint(_("Ending Reading and Opening Reading cannot be the same"))
		if si_total == 0 or pay_total == 0:
			frappe.throw(_("Sales and Comission amounts should be greater than 0"))
		item_cp = []
		for d in self.pump_meter_items:
			item_cp.append({
				'item_cd':d.pump_item_code
			})
			
		duplicates = [item_cd for n, item_cd in enumerate(item_cp) if item_cd in item_cp[:n]]
		if duplicates:
			frappe.throw(_("Item Code cannot have duplicate values"))

	def on_submit(self):
		self.posting_date = frappe.utils.nowdate()
		self.posting_time = frappe.utils.nowtime()
		for d in self.pump_meter_items:
			pump_item = frappe.db.sql("""select name 
										from `tabPump Item Price`
										where parent = %s and item_code = %s""",
										(self.pump, d.pump_item_code))
			if pump_item:
				doc = frappe.get_doc("Pump Item Price",pump_item[0][0])
				doc.opening_qty = d.ending_qty
				doc.save()

@frappe.whitelist()
def get_rates(pump,item_code):
	pump_item = frappe.db.sql("""select ifnull(b.opening_qty,0),ifnull(b.selling_price,0),
					ifnull(b.comission_per_unit,0)
					from `tabPump` a, `tabPump Item Price` b
					where a.name = %s
					and a.name = b.parent
					and b.item_code = %s""",(pump,item_code))
	if pump_item:
		return {'opening_qty':pump_item[0][0],'selling_price':pump_item[0][1],'comm_per_unit':pump_item[0][2]}
	else:
		return {'opening_qty':0,'selling_price':0,'comm_per_unit':0}
	