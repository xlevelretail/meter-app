# -*- coding: utf-8 -*-
# Copyright (c) 2018, hello@openetech.com and contributors
# For license information, please see license.txt
from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _
from frappe.utils import nowdate

class PumpEmployeeWagesJournal(Document):
	def validate(self):
		#validate for one month entry and do not all recreation of jes
		if self.from_date > self.to_date:
			frappe.throw(_("From date cannot be greater than to date"))

	def on_submit(self):
		jv = frappe.new_doc("Journal Entry")
		jv.posting_date = nowdate()
		jv.company = self.company
		jv.user_remark = "Wages Payable Entry from {0} to {1}".format(self.from_date,self.to_date)
		wage_expense = frappe.db.sql("""select ifnull(sum(commision_amount),0)
						from `tabPump Meter Reading` a, `tabPump Meter Items` b 
						where a.company = %s and a.name = b.parent and a.docstatus = 1 
						and a.posting_date >= %s and a.posting_date <= %s
						and ifnull(a.journal_entry, '') =''""",
						(self.company,self.from_date,self.to_date))
		if wage_expense:
			expense_amount = wage_expense[0][0]
			if expense_amount != 0:
				jv.set("accounts", [
					{"account":self.payroll_suspense_account,
					"credit_in_account_currency": expense_amount,
					"debit_in_account_currency": 0,"exchange_rate": 1
					},
					{"account": self.salary_expense_account,
					"debit_in_account_currency": expense_amount,
					"credit_in_account_currency": 0,"exchange_rate": 1
					}])
				jv.insert(ignore_permissions=True)
				jv.submit()
				update_status = frappe.db.sql("""update `tabPump Meter Reading` 
												set journal_entry = %s
												where company = %s and docstatus = 1 
												and posting_date >= %s and posting_date <= %s
												and ifnull(journal_entry, '') =''""",
												(jv.name,self.company,self.from_date,self.to_date))
		else:
			frappe.throw(_("Journal Entry cannot be created"))
