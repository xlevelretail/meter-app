// Copyright (c) 2018, hello@openetech.com and contributors
// For license information, please see license.txt

frappe.ui.form.on('Pump Employee Wages Journal', {
	setup: function(frm) {
		frm.set_query("salary_expense_account", function() {
			return {
				filters: {
					"company": frm.doc.company
				}
			}
		}),
		frm.set_query("payroll_suspense_account", function() {
			return {
				filters: {
					"company": frm.doc.company
				}
			}
		});
	}
});
