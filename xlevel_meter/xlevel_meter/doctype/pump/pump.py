# -*- coding: utf-8 -*-
# Copyright (c) 2018, hello@openetech.com and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _

class Pump(Document):
	def validate(self):
		item_cp = []
		for d in self.item_prices:
			item_cp.append({
				'item_cd':d.item_code
			})
			
		duplicates = [item_cd for n, item_cd in enumerate(item_cp) if item_cd in item_cp[:n]]
		if duplicates:
			frappe.throw(_("Item Code cannot have duplicate values"))
