# Copyright (c) 2013, hello@openetech.com and contributors
# For license information, please see license.txt
from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters):
	columns, data = [], []
	columns = get_columns()
	data=get_sales_comission_data(filters, columns)
	return columns, data

def get_columns():
	return [
		_("Company") + ":Link/Company:100",
		_("Pump") + ":Data:100",
		_("Employee") + ":Link/Employee:150",
		_("Date") + ":Date:100",
		_("Item") + ":Link/Item:125",
		_("Sold Qty") + ":Float:150",
		_("Sales") + ":Currency:150",
		_("Commission") + ":Currency:150",
		_("JE") + ":Link/Journal Entry:75"
	]

def get_conditions(filters):
	conditions = " 1=1 "
	if filters.get("from_date"): conditions += "and a.posting_date >= %(from_date)s"
	if filters.get("to_date"): conditions += " and a.posting_date <= %(to_date)s"
	return conditions

def get_sales_comission_data(filters, columns):
	conditions = get_conditions(filters)
	return frappe.db.sql("""
		select
			a.company as "Company",
			a.pump as "Pump",
			a.employee as "Employee",
			a.posting_date as "Date",
			b.pump_item_code as "Item",
			(b.ending_qty - b.opening_qty) as "Sold Qty",
			b.sales_amount as "Sales",
			b.commision_amount as "Commission",
			a.journal_entry as "JE"
			from `tabPump Meter Reading` a, `tabPump Meter Items` b 
		where a.name = b.parent and a.docstatus = 1 
		and {conditions}
	""".format(conditions=conditions),filters, as_dict=1)